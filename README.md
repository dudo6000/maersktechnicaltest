# MaerskTechnicalTest

Egg Timer test solution

Build code
----------
	Build code with mvn from command line or by the IDE

Drivers
-------
	Please use the latest firefox and chrome drivers
	Please use 1.8 java

Limitations
-----------
	The tests run only in chrome and firefox browsers.

Bug in the Egg application for bonus point
------------------------------------------
	The timer is starting(you can see it the title) but the text does not appear just a few minutes later.
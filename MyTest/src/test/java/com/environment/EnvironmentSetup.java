package com.environment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.enums.BrowserType;

public class EnvironmentSetup {

	private String chromeDriverPath = "C:\\chromedriver.exe";
	private String geckoDriverPath = "c:\\geckodriver.exe";
	private String firefoxPath = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";

	private WebDriver driver;
	
	public WebDriver getDriver(BrowserType browserType) {
		if(browserType.equals(BrowserType.chrome)) {
			setChromeDriver();
		} else if(browserType.equals(BrowserType.firefox)) {
			setFirefoxDriver();
		}
		
		return driver;
	}
	
	private void setChromeDriver() {
		System.setProperty("webdriver.chrome.driver",chromeDriverPath);
		driver =  new ChromeDriver();
	}
	
	private void setFirefoxDriver() {	
		System.setProperty("webdriver.gecko.driver",geckoDriverPath);
		System.setProperty("webdriver.firefox.bin", firefoxPath);
		driver =  new FirefoxDriver();				
	}
}

package com.mytest;

import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import com.enums.BrowserType;
import com.environment.EnvironmentSetup;
import com.pages.EggTimerCountingPage;
import com.pages.EggTimerSettingsPage;


public class CountDownChromeTest {

	private static WebDriver driver;
	private int seconds = 25;
	EggTimerSettingsPage etsp = new EggTimerSettingsPage();
    EggTimerCountingPage egtcp = new EggTimerCountingPage();
	
	@BeforeClass
	public static void openBrowser(){
		EnvironmentSetup es = new EnvironmentSetup();
		driver = es.getDriver(BrowserType.chrome);
	}

	@Test
	public void checkTimerByTitle() throws InterruptedException{
		
        etsp.openEggPage(driver);
        
        assertTrue("Title is not correct!", etsp.checkTitle(driver));        

        assertTrue("Setted value is not good in the timer text field!", etsp.setTimerTextField(driver, seconds));
        
        etsp.startTimer(driver);
                
        assertTrue("Timer counter failed!",egtcp.validateCountingByTitle(driver, seconds));
        
        assertTrue("Alert text checking failed!",egtcp.acceptPopupWindow(driver));
        
        assertTrue("Expired message failed!", egtcp.checkTimeExpired(driver,5));

  	}

	@Test
	public void checkTimerByText() throws InterruptedException{
		
        etsp.openEggPage(driver);
        
        assertTrue("Title is not correct!", etsp.checkTitle(driver));        

        assertTrue("Setted value is not good in the timer text field!", etsp.setTimerTextField(driver, seconds));
        
        etsp.startTimer(driver);
                
        assertTrue("Timer counter failed!",egtcp.validateCountingByText(driver, seconds));
        
        assertTrue("Alert text checking failed!",egtcp.acceptPopupWindow(driver));
        
        assertTrue("Expired message failed!", egtcp.checkTimeExpired(driver,5));
        
  	}
	
	@AfterClass
	public static void closeBrowser(){
		driver.close();
	    driver.quit();
	}
}

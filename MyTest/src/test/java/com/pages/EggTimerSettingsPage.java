package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EggTimerSettingsPage {

	private String page = "http://e.ggtimer.com/";
	private String timerTextFieldXpath = "//*[@id='start_a_timer']";
	private String goTimerButtonXpath = "//*[@id='timergo']";
	
	private String tilteText = "E.gg Timer - a simple countdown timer";
	
	public void openEggPage(WebDriver driver) {
		driver.get(page);
	}
	
	public boolean checkTitle(WebDriver driver) {
		return driver.getTitle().equals(tilteText);
	}
	
	public boolean setTimerTextField(WebDriver driver, int seconds) {
		WebElement 	timerTextField = (new WebDriverWait(driver, 10))
     		   .until(ExpectedConditions.elementToBeClickable(By.xpath(timerTextFieldXpath)));
		
		timerTextField.clear();
        timerTextField.sendKeys(Integer.toString(seconds));
        
        return timerTextField.getAttribute("value").equals(Integer.toString(seconds));
	}
	
	public void startTimer(WebDriver driver) {
		WebElement goTimerButton = (new WebDriverWait(driver, 10))
	     		   .until(ExpectedConditions.elementToBeClickable(By.xpath(goTimerButtonXpath)));
	        
	    goTimerButton.click();
	}
}

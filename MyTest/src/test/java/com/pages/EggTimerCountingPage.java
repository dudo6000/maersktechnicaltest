package com.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.helper.Helper;

public class EggTimerCountingPage {
	
	private String countingTextXpath = "//*[@id='progressText']";
	
	private String timerExpiredMessage = "Time Expired!";
	
	public boolean validateCountingByTitle(WebDriver driver, int timeout) {
		
		String currentTime = driver.getTitle();
		
		int minute = Integer.parseInt(currentTime.substring(0,2));
        int second = Integer.parseInt(currentTime.substring(3,5));
        
        String secondString;
        String minuteString;
        
        for(int i=0; i<timeout; ++i) {
        	long start_time = System.nanoTime();
        	
        	
        	if(second == 0 && minute == 0) {
        		return true;
        	}
        	
        	secondString = Helper.formatMinutOrSecond(second);
        	minuteString = Helper.formatMinutOrSecond(minute);        	
        	
        	if(!driver.getTitle().equals(minuteString + ":" + secondString + " - E.ggtimer")) {
        		return false;
        	}
        	
        	if(second == 0) {
        		second = 59;
        		--minute;
        	} else {
        		--second;
        	}
        	
        	long end_time = System.nanoTime();
        	double difference = (end_time - start_time) / 1e6;
        	
        	try {
				Thread.sleep(1000-(int)Math.round(difference));
			} catch (InterruptedException e) {
			}
        }
        
		return true;
	}

	public boolean validateCountingByText(WebDriver driver, int timeout) {
		
    	WebElement timerText = (new WebDriverWait(driver, 10))
      		   .until(ExpectedConditions.elementToBeClickable(By.xpath(countingTextXpath)));
		
		String currentTimeText = timerText.getText();
		
        int second = Integer.parseInt(currentTimeText.substring(0,2));
        
        String secondLabelValue;
        
        for(int i=0; i<timeout; ++i) {
        	 long start_time = System.nanoTime();
        	
        	secondLabelValue = Helper.secondLabelValue(second);

			if (!timerText.getText().equals(secondLabelValue)) {
				return false;
			}
			
        	--second;
        	
        	if(second == 0) {
        		break;
        	}
        	
        	long end_time = System.nanoTime();
        	double difference = (end_time - start_time) / 1e6;

        	try {
				Thread.sleep(1000-(int)Math.round(difference));
			} catch (InterruptedException e) {
			}
        }
        
		return true;
	}

	
	
	public boolean checkTimeExpired(WebDriver driver, int timeout) {
		
		WebElement timerText = (new WebDriverWait(driver, 10))
	      		   .until(ExpectedConditions.elementToBeClickable(By.xpath(countingTextXpath)));
		
		for(int i=0; i<timeout;++i) {
			if(timerText.getText().equals(timerExpiredMessage)) {
				return true;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
		
		return false;
	}
	
	public boolean acceptPopupWindow(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        
        boolean checkText = alert.getText().equals(timerExpiredMessage);
        
        alert.accept();
        return checkText;
	}
}

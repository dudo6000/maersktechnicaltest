package com.helper;

public class Helper {
	public static String formatMinutOrSecond(int input) {
		if(input <10) {
        	return "0"+Integer.toString(input);
        } else {
        	return Integer.toString(input);
        }
	}
	
	public static String minuteLabelValue(int minute) {
		switch (minute) {
		case 0:
			return "";
		case 1:
			return "1 minute";
		default:
			return minute + " minutes";
		}
	}
	
	public static String secondLabelValue(int minute) {
		switch (minute) {
		case 0:
			return "";
		case 1:
			return "1 second";
		default:
			return minute + " seconds";
		}
	}
}
